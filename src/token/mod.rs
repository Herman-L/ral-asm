use std::fmt::{self, Display, Formatter};

mod span;
mod stream;
pub use span::Span;
pub use stream::Stream;

/// A single token in the source code.
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub enum Token<'a> {
    /// A Ral opcode.
    Opcode(u8),
    /// A label.
    Label(&'a str),
    /// Start of a label definition (`[`).
    StartDefine,
    /// End of a label definition (`]`).
    EndDefine,
    /// An unknown character in the source.
    Unknown(char),
    /// The end of file.
    End,
}

impl Display for Token<'_> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match *self {
            Token::Opcode(chr) => write!(f, "{:?}", chr as char),
            Token::Label(label) => write!(f, "'{}'", label),
            Token::StartDefine => write!(f, "'['"),
            Token::EndDefine => write!(f, "']'"),
            Token::Unknown(chr) => write!(f, "{:?}", chr),
            Token::End => write!(f, "end of file"),
        }
    }
}
