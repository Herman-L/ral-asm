use crate::token::Span;
use std::cmp::max;
use unicode_width::UnicodeWidthStr;

#[allow(clippy::module_name_repetitions)]
#[derive(Debug)]
pub struct ParseError {
    pub message: String,
    pub span: Span,
}
impl ParseError {
    pub const fn new(message: String, span: Span) -> Self {
        Self {
            message,
            span,
        }
    }
    pub fn display(&self, mut source: &str) {
        let mut offset = 0;
        let mut line_nr = 1;

        while let Some(index) = source.find('\n') {
            if index + offset < self.span.0 {
                offset += index + 1;
                source = &source[index + 1..];
                line_nr += 1;
            } else {
                break;
            }
        }
        let line = source.lines().next().unwrap_or("");

        let span_start = padded_width(line, self.span.0 - offset);
        let span_end = padded_width(line, self.span.1 - offset);
        let highlight = " ".repeat(span_start) + &"^".repeat(max(1, span_end - span_start));

        let l_pad = " ".repeat(line_nr.to_string().len());
        eprintln!("\x1B[1;31mError:\x1B[0m {}", self.message);
        eprintln!("\x1B[1;34m{} |\x1B[0m", l_pad);
        eprintln!("\x1B[1;34m{} |\x1B[0m {}", line_nr, line);
        eprintln!("\x1B[1;34m{} |\x1B[31m {}\x1B[0m", l_pad, highlight);
    }
}

fn padded_width(line: &str, upto: usize) -> usize {
    if upto < line.len() {
        line[..upto].width()
    } else {
        line.width() + (upto - line.len())
    }
}
