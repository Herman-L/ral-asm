# Ral Assembler

This is a simple [Ral](https://gitlab.com/Herman-L/ral) assembler.

## Example

The following cat program:

    1 Condition? [Loop] . [Condition] , : Loop?

Assembles to (spaces added for clarity):

    1 11:++:+:+? . , : 111:+:++:++?

## Usage

    USAGE:
        ral-asm [OPTIONS] [input]

    FLAGS:
        -h, --help       Prints help information
        -V, --version    Prints version information

    OPTIONS:
        -o, --output <output>    Output file, stdout if not present
            --wrap <wrap>        Width of lines in the output. 0 disables wrapping. [default: 80]

    ARGS:
        <input>    Input file, stdin if not present
